# pandoc Markdown → HTML diapo exemplaire avec KaTeX

Voir le [diapo][diapo] pour en savoir plus.

# pandoc Markdown → HTML slides sample with KaTeX

See the [slides][slides] to know more.

[diapo]: https://vincenttam.gitlab.io/html-slides-exemplaire/
[slides]: https://vincenttam.gitlab.io/html-slides-exemplaire/index-en.html
