document.addEventListener("DOMContentLoaded", () => {
  const diapo = document.querySelector("#mes-propres-trucs");
  const btn = diapo.querySelector("button");
  const liste = diapo.querySelector("ul");
  const noeud = document.createElement("li");
  noeud.textContent = "Bouge ta souris.";
  btn.addEventListener("mouseenter", () => {
    liste.appendChild(noeud);
  });
  btn.addEventListener("mouseleave", () => {
    liste.removeChild(liste.lastChild);
  });
});
