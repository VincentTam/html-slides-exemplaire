document.addEventListener("DOMContentLoaded", () => {
  const slides = document.querySelector("#my-own-stuff");
  const btn = slides.querySelector("button");
  const list = slides.querySelector("ul");
  const node = document.createElement("li");
  node.textContent = "Move your mouse.";
  btn.addEventListener("mouseenter", () => {
    list.appendChild(node);
  });
  btn.addEventListener("mouseleave", () => {
    list.removeChild(list.lastChild);
  });
});
