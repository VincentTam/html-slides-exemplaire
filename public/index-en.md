---
title: Slides for the 3rd day
author: <a href="https://git.io/vtam">Vincent Tam</a>
date: 29th March 2023
header-includes: |
  <link rel="stylesheet" href="style-en.css">
  <script src="script-en.js"></script>
---

# Base Conversion

- [$\KaTeX$][katex] works?
- Let's see:

    $$\begin{aligned}
      500 &= 1 \times 256 + 244 \\
      &= {\color{blue}1} \times 16^{\color{red}2} + 240 + 4 \\
      &= {\color{blue}\underset{\substack{\downarrow\\[2pt]1_{16}}}{1}}
        \times 16^{\color{red}2} +
        {\color{blue}\underset{\substack{\downarrow\\[2pt]\mathrm{F}_{16}}}{15}}
        \times 16^{\color{red}1} +
        {\color{blue}\underset{\substack{\downarrow\\[2pt]4_{16}}}{4}}
        \times 16^{\color{red}0} \\
      &= {\color{blue}1\mathrm{F}4_{16}}
    \end{aligned}$$

    $$\begin{array}{c|c|c|c}
      {\color{red}\text{power }} 16^{\color{red}?} &
      {\color{red}2} & {\color{red}1} & {\color{red}0} \\\hline
      {\color{blue}\text{hexadecimal digits } ?}_{16} &
      {\color{blue}1} & {\color{blue}\mathrm{F}} & {\color{blue}4}
    \end{array}$$

# Tech used

[Markdown][md] → HTML slides made with [pandoc][pandoc]

```sh
pandoc -s --katex -i -t slidy index.md -o index.html
```

It works on any mainstream browser, and mobile also.  That's it for the basics.
**You can stop here** if you aren't seeking to make avanced things.

[Markdown][md] code of these slides: view the file
[`index-en.md` in my repo][src].

<div class="small-image">
  ![Git Bash Vim](vim.png)
  ![Git Bash](bash.png)
</div>

# My own stuff

- `style.css` for custom styles (see next slide)
- `script.js` with a special <button>button</button> (move your mouse on top of
it)

    ```js
    document.addEventListener("DOMContentLoaded", () => {
      const slides = document.querySelector("#my-own-stuff");
      const btn = slides.querySelector("button");
      const list = slides.querySelector("ul");
      const node = document.createElement("li");
      node.textContent = "Move your mouse.";
      btn.addEventListener("mouseenter", () => {
        list.appendChild(node);
      });
      btn.addEventListener("mouseleave", () => {
        list.removeChild(list.lastChild);
      });
    });
    ```

# Skill: header

Specify the custom scripts and styles to be used in the source file's header.

```html
---
header-includes: |
  <script src="script.js"></script>
  <link rel="stylesheet" href="style.css">
---

my markdown document
```

Source: <https://stackoverflow.com/a/42371554/3184351>

# Skill: wrapper

<div class="my-wrapper">
  1. [pandoc][pandoc] doesn't support custom styles for HTML output.
  1. Wrap the <span>target</span> with a « `<div>` wrapper ».  For example:
</div>

- Markdown (`index.md`)

    ```html
    <div class="my-wrapper">
      1. Wrap the <span>target</span>…
      1. …
    </div>
    ```

- CSS (`style.css`)

    ```css
    .my-wrapper span {
      display: inline-block;
      border: 2px solid red;
      padding: .15em .2em;
    }
    ```

Source: <https://stackoverflow.com/a/45306645/3184351>

[katex]: https://katex.org/
[md]: https://www.markdowntutorial.com/
[src]: https://gitlab.com/VincentTam/html-slides-exemplaire/-/blob/master/public/index-en.md
[pandoc]: https://pandoc.org/
