---
title: Diapo pour la 3e journée
author: <a href="https://git.io/vtam">Vincent Tam</a>
date: 29 mars 2023
header-includes: |
  <link rel="stylesheet" href="style.css">
  <script src="script.js"></script>
---

# Conversion de base

- [$\KaTeX$][katex] marche ?
- on va voir:

    $$\begin{aligned}
      500 &= 1 \times 256 + 244 \\
      &= {\color{blue}1} \times 16^{\color{red}2} + 240 + 4 \\
      &= {\color{blue}\underset{\substack{\downarrow\\[2pt]1_{16}}}{1}}
        \times 16^{\color{red}2} +
        {\color{blue}\underset{\substack{\downarrow\\[2pt]\mathrm{F}_{16}}}{15}}
        \times 16^{\color{red}1} +
        {\color{blue}\underset{\substack{\downarrow\\[2pt]4_{16}}}{4}}
        \times 16^{\color{red}0} \\
      &= {\color{blue}1\mathrm{F}4_{16}}
    \end{aligned}$$

    $$\begin{array}{c|c|c|c}
      {\color{red}\text{puissance }} 16^{\color{red}?} &
      {\color{red}2} & {\color{red}1} & {\color{red}0} \\\hline
      {\color{blue}\text{chiffres hexadécimal } ?}_{16} &
      {\color{blue}1} & {\color{blue}\mathrm{F}} & {\color{blue}4}
    \end{array}$$

# Techo utilisées

[Markdown][md] → HTML diapo fait avec [pandoc][pandoc]

```sh
pandoc -s --katex -i -t slidy index.md -o index.html
```

Ça marche sur n'importe quel grand browser, et mobile aussi.  Voilà l'essentiel.
**Tu peux t'arrêter là** si tu cherches pas à faire des trucs avancés.

Code [Markdown][md] de ce diapo : voir le fichier
[`index.md` dans mon dépôt][src].

<div class="petite-image">
  ![Git Bash Vim](vim.png)
  ![Git Bash](bash.png)
</div>

# Mes propres trucs

- `style.css` pour des styles personnalisés (à voir dans le prochain diapo)
- `script.js` avec une <button>bouton</button> spécial (pose ta souris dessus)

    ```js
    document.addEventListener("DOMContentLoaded", () => {
      const diapo = document.querySelector("#mes-propres-trucs");
      const btn = diapo.querySelector("button");
      const liste = diapo.querySelector("ul");
      const noeud = document.createElement("li");
      noeud.textContent = "Bouge ta souris.";
      btn.addEventListener("mouseenter", () => {
        liste.appendChild(noeud);
      });
      btn.addEventListener("mouseleave", () => {
        liste.removeChild(liste.lastChild);
      });
    });
    ```

# Astuce header

Préciser les scripts et les styles personnalisés à utiliser dans la tête du
fichier source.

```html
---
header-includes: |
  <script src="script.js"></script>
  <link rel="stylesheet" href="style.css">
---

mon document markdown
```

Source: <https://stackoverflow.com/a/42371554/3184351>

# Astuce wrapper

<div class="mon-wrapper">
  1. [pandoc][pandoc] ne supporte pas les styles personnalisés pour la sortie en
    HTML.
  1. Entourner la <span>cible</span> avec un « `<div>` wrapper ».  Par example :
</div>

- Markdown (`index.md`)

    ```html
    <div class="mon-wrapper">
      1. Entourner la <span>cible</span>…
      1. …
    </div>
    ```

- CSS (`style.css`)

    ```css
    .mon-wrapper span {
      display: inline-block;
      border: 2px solid red;
      padding: .15em .2em;
    }
    ```

Source: <https://stackoverflow.com/a/45306645/3184351>

[katex]: https://katex.org/
[md]: https://www.markdowntutorial.com/
[src]: https://gitlab.com/VincentTam/html-slides-exemplaire/-/blob/master/public/index.md
[pandoc]: https://pandoc.org/
